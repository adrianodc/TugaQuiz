
# TugaQuiz

**Simple project to test NGX-Translate, Angular Material and Angular 6.**

This small project shows a group of questions with words in Portuguese (Portugal) and possible equivalents in Portuguese (Brazil).

At the end, the app shows a feedback about "how Portuguese" the user is based on that.
In future implementations, the questionnaire will also be exhibited in English (US).

To get in contact with the author, go to [Un caffè per due](https://uncaffeperdue.com).
