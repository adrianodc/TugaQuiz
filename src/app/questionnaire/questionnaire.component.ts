import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {QuestionsService} from '../services/questions.service';
import { Question } from '../shared/models/question.model';
import { Answer } from '../shared/models/answer.model';
import { Subscription } from 'rxjs';
import { QuestionList } from '../shared/models/question.list.model';
import { Router } from '@angular/router';
import { faCrown, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import {faSmile, faSadTear } from '@fortawesome/free-regular-svg-icons';

const NUM_OF_QUESTIONS = 10;

export enum Step {
  ANSWERING = 'ANSWERING',
  RESULT = 'RESULT'
}

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.css']
})
export class QuestionnaireComponent implements OnInit, OnDestroy {
  private questions: Question[];
  private observe: Subscription;
  private answers: Answer[];
  private questionsIndexes: number[];
  public step: Step;
  private displayedColumns: string[] = ['prt', 'rightAnswer', 'yourAnswer'];
  private result: number;
  private faCrown = faCrown;
  private faSmile = faSmile;
  private faSadTear = faSadTear;
  private faTimesCircle = faTimesCircle;


  constructor(private questionsService: QuestionsService, private router: Router) { }

  ngOnInit() {
    this.getQuestions();
    this.step = Step.ANSWERING;
  }

  ngOnDestroy() {
    this.observe.unsubscribe();
  }

  finishAnswering() {
    this.step = Step.RESULT;
    this.result = 0;
    this.answers.forEach(answer => {
      if (answer.answer === answer.question.answer) {
        this.result++;
      }
    });
    window.scrollTo(window.screenTop);
  }

  seeAnswerKey() {
    this.router.navigateByUrl('/list-answer-key');
  }

  resetGame() {
    this.generateQuestionnaire();
    this.step = Step.ANSWERING;
    window.scrollTo(window.screenTop);
  }

  getQuestions() {
    this.observe = this.questionsService.getQuestions()
      .subscribe((data: QuestionList) => {
        this.questions = data.questions;
        this.generateQuestionnaire();
      });
  }

  generateQuestionnaire() {
    this.questionsIndexes = [];
    this.answers = [];
    while (this.questionsIndexes.length < NUM_OF_QUESTIONS) {
      const num = this.getRandomNumber(this.questions.length);
      if (! (this.questionsIndexes.find(x => x === num))) {
        this.questionsIndexes.push(num);
        const answer = new Answer();
        answer.question = this.questions[num];
        this.answers.push(answer);
      }
    }
  }

  /**
   * Returns a number between 0 and max.
   * @param max Maximum value.
   */
  getRandomNumber(max: number): number {
    return Math.floor(Math.random() * Math.floor(max));
  }
}
