import {Question} from './question.model';

export class QuestionList {
    questions: Array<Question>;
}