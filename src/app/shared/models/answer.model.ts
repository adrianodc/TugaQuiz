import { Question } from "./question.model";

export class Answer {
    question: Question;
    answer: string;
}
