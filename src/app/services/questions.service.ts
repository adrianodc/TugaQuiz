import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { QuestionList } from '../shared/models/question.list.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {
  private questionsURL = './assets/questions/questions.json';
    constructor(private http: HttpClient) {}

  getQuestions(): Observable<QuestionList> {
    return this.http.get<QuestionList>(this.questionsURL);
  }
}
