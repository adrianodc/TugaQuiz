import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Question } from '../shared/models/question.model';
import { QuestionList } from '../shared/models/question.list.model';
import {QuestionsService} from '../services/questions.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tuga-list-answer-key',
  templateUrl: './tuga-list-answer-key.component.html',
  styleUrls: ['./tuga-list-answer-key.component.css']
})
export class TugaListAnswerKeyComponent implements OnInit, OnDestroy {
  public questions: Question[] = [];
  private observe: Subscription;
  public displayedColumns: string[] = ['prt', 'rightAnswer'];//'option1', 'option2', 'option3', 'option4'];
  constructor(private questionsService: QuestionsService, private router: Router) { }

  ngOnInit() {
    this.getQuestions();
  }

  tryAgain() {
    this.router.navigateByUrl('/questionnaire');
  }

  getQuestions() {
    this.observe = this.questionsService.getQuestions()
      .subscribe((data: QuestionList) => {
        this.questions = data.questions;
      });
  }

  ngOnDestroy() {
    this.observe.unsubscribe();
  }
}
