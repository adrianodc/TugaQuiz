import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TugaListAnswerKeyComponent } from './tuga-list-answer-key.component';

describe('TugaListAnswerKeyComponent', () => {
  let component: TugaListAnswerKeyComponent;
  let fixture: ComponentFixture<TugaListAnswerKeyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TugaListAnswerKeyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TugaListAnswerKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
