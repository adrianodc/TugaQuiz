import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TugaListAnswerKeyComponent} from '../tuga-list-answer-key/tuga-list-answer-key.component';
import { WelcomeScreenComponent } from '../welcome-screen/welcome-screen.component';
import { QuestionnaireComponent } from '../questionnaire/questionnaire.component';

const routes: Routes = [
  { path: '', redirectTo: '/welcome-screen', pathMatch: 'full' },
  { path: 'welcome-screen', component: WelcomeScreenComponent },
  { path: 'questionnaire', component: QuestionnaireComponent },
  { path: 'list-answer-key', component: TugaListAnswerKeyComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
