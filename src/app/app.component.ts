import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit {
  public selectedLanguage: string;

  constructor(private translate: TranslateService) {
  }
  ngOnInit() {
    this.selectedLanguage = 'pt_BR';
    this.translate.setDefaultLang(this.selectedLanguage);
  }

  updateLanguage(language: string) {
    this.selectedLanguage = language;
    this.translate.use(language);
  }
}
